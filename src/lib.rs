#![deny(missing_docs)]
/*!
See [README.md](https://crates.io/crates/sylasteven-system-pns)
**/

use lazy_static::lazy_static;

use sylasteven as engine;

use glutin::{GlContext, GlWindow};
use nanovg::{Font, Frame, Image};

pub use nanovg;

use std::collections::HashMap as Map;

use std::path::PathBuf;

struct Context(nanovg::Context);

unsafe impl Sync for Context {}

lazy_static! {
    static ref NANOVG: Context = Context(
        nanovg::ContextBuilder::new()
            .stencil_strokes()
            .build()
            .expect("Initialization of NanoVG failed!")
    );
}

/// The user interface render event.
pub struct RenderEvent {
    /// The frame used for UI rendering.
    pub frame: Frame<'static>,
    /// The default font for UI rendering.
    pub default_font: Font<'static>,
}

/// The user interface render system.
pub struct Render {
    default_font: Font<'static>,
    /// All loaded images mapped by their file path.
    pub images: Map<std::path::PathBuf, Image<'static>>,
    gl_window: GlWindow,
}

impl Render {
    /// Creates the render system by taking a GlWindow from glutin?
    pub fn new(gl_window: GlWindow) -> Render {
        let default_font = Font::from_file(
            &NANOVG.0,
            "Default",
            concat!(env!("CARGO_MANIFEST_DIR"), "/resources/FreeMono.ttf"),
        )
        .expect("Failed to load font 'FreeMono.ttf'");
        Self {
            default_font,
            gl_window,
            images: Map::new(),
        }
    }

    /// Adds new images and maps them to their file path.
    /// When adding the same file multiple times, the image is not loaded again.
    pub fn add_image(&mut self, path: &PathBuf) -> Option<PathBuf> {
        let context = &NANOVG.0;
        if self.images.get(path).is_some() {
            Some(path.clone())
        } else if let Some(image) = Image::new(context).build_from_file(&path).ok() {
            let result = Some(path.clone());
            self.images.insert(path.to_path_buf(), image);
            result
        } else {
            None
        }
    }
}

use crate::engine::{Handler, System};

impl<H: Handler> System<H> for Render
where
    H::Event: From<RenderEvent>,
{
    fn process(&mut self, handler: &mut H) {
        let context = &NANOVG.0;
        let Self {
            gl_window,
            default_font,
            ..
        } = self;
        let (width, height) = gl_window.get_inner_size().expect("Oh no");
        let (width, height) = (width as i32, height as i32);

        unsafe {
            gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT | gl::STENCIL_BUFFER_BIT);
            gl::Viewport(0, 0, width, height);
        }

        let (width, height) = (width as f32, height as f32);

        context.frame((width, height), gl_window.hidpi_factor(), |frame| {
            handler.handle(
                RenderEvent {
                    frame,
                    default_font: *default_font,
                }
                .into(),
            )
        });
        gl_window.swap_buffers().unwrap();
    }
}
